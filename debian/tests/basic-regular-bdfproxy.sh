#!/bin/sh -e
##
## basic-regular-bdfproxy.sh
##
## Made by phil
## Login   <phil@reseau-libre.net>
##
## Started on  Wed 28 Jun 2017 11:10:04 AM CEST phil
## Last update Wed 28 Jun 2017 11:20:48 AM CEST pret
##

cd $(AUTOPKGTEST_TMP)

bdfproxy &

# by default, bdfproxy listen on port 8080
netstat -laputen 2>/dev/null|grep 8080

